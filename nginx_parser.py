import re
import datetime as DT

#parser function for one line
def parser(line: str, patterns: dict, delim: str) -> str:
    resultline = []
    line_len = len(line)
    i = 0
    while (i < line_len):
        for key, pattern in patterns.items():
            pat = re.compile(pattern)
            m = pat.search(line, i)
            if m is not None:
                if key == 'time':
                    dt = DT.datetime.strptime(m.group(1), '%d.%m.%Y %H:%M:%S %z').isoformat()
                    val = str(dt)
                else:
                    val = str(m.group(1))
                resultline.append(val)
                i = m.end()
    return delim.join(resultline)

# Шаблоны
patterns = {'remote_addr': '(\S+)',
       'remote_user': '(\S+)',
       'time': '\[([^\]]+)\]',
       'request': '"([^"]+)"',
       'status': '(\d+)',
       'body_bytes_sent': '(\d+)',
       'http_referer': '"([^"]+)"',
       'http_user_agent': '("[^"]+")'}

# Указываем имя файла с логами и итогового файла
filename = "logs_nginx.csv"
result_file = "logs_nginx_parsed.csv"
delim = ','
headers = list(patterns)
headers_len = len(headers) - 1

#Read and write parsed data
with open(filename, 'r') as fr, open(result_file, 'w') as fw:

    for key in headers:
        if key == headers[headers_len]:
            fw.write(key + '\n')
        else:
            fw.write(key + delim)

    for line in fr:
        parser_line = parser(line.rstrip('\n'), patterns, delim)
        fw.write(parser_line + '\n')





